# import two coordinate sets and cubic pbc value
# two atoms to determine distance between are arrays with floats as [x,y,z]
def get_min_image( ref_atom,move_atom,pbc_size ):
	# check each dimension to see if closest pair is through pbc
	if abs(ref_atom[0] - move_atom[0]) > (pbc_size/2.0):
		if ref_atom[0] > move_atom[0]:
			move_atom[0] = move_atom[0] + pbc_size
		else:
			move_atom[0] = move_atom[0] - pbc_size
	if abs(ref_atom[1] - move_atom[1]) > (pbc_size/2.0):
		if ref_atom[1] > move_atom[1]:
			move_atom[1] = move_atom[1] + pbc_size
		else:
			move_atom[1] = move_atom[1] - pbc_size
	if abs(ref_atom[2] - move_atom[2]) > (pbc_size/2.0):
		if ref_atom[2] > move_atom[2]:
			move_atom[2] = move_atom[2] + pbc_size
		else:
			move_atom[2] = move_atom[2] - pbc_size
	return move_atom
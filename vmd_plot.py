from subprocess import call

def make_vmd_plot(sys,mol_type_1,mol_type_2,t):
	# set vmd executable
	vmd_exec = 'vmd'
	# run vmd with those mol files and the representation file
	call([vmd_exec,'-f','../coords/%s_%s.gro' % (mol_type_1,t),'-f','../coords/%s_%s.gro' % (mol_type_2,t),'-e','representation_%s.txt' % t])
	return


# mol ids and coords and cubic pbc value for community
# return the pddf and radius of gyration for that community
import numpy as np
from minimum_image import get_min_image

def get_pddf_and_rg( comm_mol_coords, pbc_size ):
	comm_dist_sum = 0.0
	comm_pair_dist_list = []
	ref_pos = []
	for comm_mol_1 in range(len(comm_mol_coords)):
			for comm_mol_2 in range(len(comm_mol_coords)):
				if comm_mol_1 != comm_mol_2:
					ref_pos = comm_mol_coords[comm_mol_1]
					new_pos = get_min_image( ref_pos, comm_mol_coords[comm_mol_2], pbc_size)
					pair_dist = np.sqrt((ref_pos[0] - new_pos[0])**2.0 + (ref_pos[1] - new_pos[1])**2.0 + (ref_pos[2] - new_pos[2])**2.0)
					comm_dist_sum += pair_dist**2.0
					if len(comm_pair_dist_list) == 0:
						comm_pair_dist_list.append([pair_dist])
					else:
						comm_pair_dist_list += [pair_dist]
	# define radius of gyration for cluster of a given size as 1/sqrt(1/2) times the rmsd of all pairs within the cluster divided by the cluster size squared
	# rather than using a COM distance
	comm_rg = np.sqrt(0.5*comm_dist_sum/float(len(comm_mol_coords))**2.0)
	comm_pddf, comm_pddf_bin_edges = np.histogram(comm_pair_dist_list, bins=100, range=(0, 10), density=True)
	comm_pddf_bins = []
	# get bin centers from bin edges
	for bin_val in range(0,len(comm_pddf_bin_edges) - 1,1):
		comm_pddf_bins += [(comm_pddf_bin_edges[bin_val] + comm_pddf_bin_edges[bin_val + 1])/2.0]
	return comm_pddf, comm_pddf_bins, comm_rg
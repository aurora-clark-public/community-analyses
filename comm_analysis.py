import os
import numpy as np
import networkx as nx
import community
import sys as sys_arg
import subprocess
import matplotlib.pyplot as plt
from vmd_plot import make_vmd_plot
from rep_file import make_rep_file
from minimum_image import get_min_image
from get_pddf import get_pddf_and_rg

def main():

	# SET SYSTEM VARIABLES
	# directory with trajectory
	sys = sys_arg.argv[1]
	# molecule types
	mol_type_1 = 'ballA'
	mol_type_2 = 'ballB'
	# trajectory and sampling parameters
	start_t = 0
	end_t = 1000000
	sampling_dt = 1000
	# choose dendorgram level to do analysis with
	dend_level = -1
	# exponent for distance weights
	dist_weights = -1.0
	# only doing analysis on mol_type_1
	# get mol numbers fro sys top file
	os.chdir(sys)
	with open('%s.top' % sys) as top_file:
		for line in top_file:
			if mol_type_1 in line:
				num_mol_1 = int(line.split()[-1])
			elif mol_type_2 in line:
				num_mol_2 = int(line.split()[-1])
	# generate coordinate files from gromacs compressed trajectory
	if os.path.exists('coords') == False:
		os.mkdir('coords')
	os.chdir('coords')
	make_gro = subprocess.Popen(('gmx','trjconv','-f','../%s.xtc' % sys,'-s','../%s.tpr' % sys,'-b','%s' % start_t,'-e','%s' % end_t,'-dt','%s' % sampling_dt,'-o','%s_.gro' % mol_type_1,'-sep'),stdin=subprocess.PIPE,stdout=subprocess.PIPE)
	stdin = make_gro.communicate('%s\n' % mol_type_1)
	make_gro = subprocess.Popen(('gmx','trjconv','-f','../%s.xtc' % sys,'-s','../%s.tpr' % sys,'-b','%s' % start_t,'-e','%s' % end_t,'-dt','%s' % sampling_dt,'-o','%s_.gro' % mol_type_2,'-sep'),stdin=subprocess.PIPE,stdout=subprocess.PIPE)
	stdin = make_gro.communicate('%s\n' % mol_type_2)
	os.chdir('..')
	# start community analysis
	if os.path.exists('comm_analysis') == False:
		os.mkdir('comm_analysis')
	os.chdir('comm_analysis')
	# get connectivity information (distances) into adjacency matrix
	# loop through simulation time steps to store graphs into dict
	t = start_t
	# initialize cluster dictionaries, list of clusters per time step
	conn_comp_dict = {}
	level_zero_modularity_list = []
	level_modularity_list = []
	num_comms_array = []
	level_zero_num_comms_array = []
	cluster_size_dist = np.zeros(num_mol_1 - 1)
	comm_size_dist = np.zeros(num_mol_1 - 1)
	level_zero_comm_size_dist = np.zeros(num_mol_1 - 1)
	level_comm_size = []
	level_zero_comm_size = []
	while t <= end_t:
		print('time: %s' % t)
		# make list with all mol_1 coords
		mol_1_coords = []
		# get coords for this time
		with open('../coords/%s_%s.gro' % (mol_type_1,int((t - start_t)/sampling_dt))) as time_gro:
			for i,line in enumerate(time_gro):
				# get mol coords for each line
				if i > 1 and i < (num_mol_1 + 2):
					line_mol = int(line[0:5])
					if line_mol <= num_mol_1:
						if len(mol_1_coords) == 0:
							mol_1_coords.append([float(line.split()[-3]),float(line.split()[-2]),float(line.split()[-1])])
						else:
							#print([float(line.split()[-3]),float(line.split()[-2]),float(line.split()[-1])])
							mol_1_coords += [[float(line.split()[-3]),float(line.split()[-2]),float(line.split()[-1])]]
							#print(mol_1_coords)
				elif i == (num_mol_1 + 2):
					pbc_size = float(line.split()[0])
		# create community adjacency matrix for this time step
		AM = np.zeros((num_mol_1,num_mol_1))
		# create cluster adjacency matrix for this time step
		cluster_AM = np.zeros((num_mol_1,num_mol_1))
		# put distances into AM
		for i in range(num_mol_1):
			for j in range(num_mol_1):
				if i != j:
					#i_pos = mol_1_coords[i]
					#print(i_pos,mol_1_coords[j])
					#j_pos = get_min_image(i_pos,mol_1_coords[j],pbc_size)
					ij_dist = np.sqrt((mol_1_coords[i][0]-get_min_image(mol_1_coords[i],mol_1_coords[j],pbc_size)[0])**2.0+(mol_1_coords[i][1]-get_min_image(mol_1_coords[i],mol_1_coords[j],pbc_size)[1])**2.0+(mol_1_coords[i][2]-get_min_image(mol_1_coords[i],mol_1_coords[j],pbc_size)[2])**2.0)
					# put weighted edges into adj mat with maximum distance of 1.2 nm (the LJ cutoff)...
					if ij_dist <= 1.2:
						AM[i][j] = (ij_dist)**dist_weights
						AM[j][i] = (ij_dist)**dist_weights
					# and unweighted nearest neighbors for unweighted clusters
					if ij_dist <= 0.55:
						cluster_AM[i][j] = 1
						cluster_AM[j][i] = 1
		# convert adjacency matrix to networkx formation
		NX_AM = nx.from_numpy_matrix(AM)
		cluster_NX_AM = nx.from_numpy_matrix(cluster_AM)
		# get the connected component subgraphs as sets in clusters
		clusters = []
		clusters_gen = nx.connected_components(cluster_NX_AM)
		for element in clusters_gen:
			clusters = clusters + [list(element)]
			# off by one because 1-indexed
			cluster_size_dist[len(list(element)) - 1] += 1.0
		conn_comp_dict['clusters_%s' % t] = clusters
		# set modularity optimization resolution paramter, 1.0 is default value
		res = 1.0
		# create the community partition from the first order dendrogram
		# use seed value of 1 for initial state for reproducibility
		dendrogram = community.generate_dendrogram(NX_AM,resolution=res,random_state=1)
		# make list of dictionaries: one for each dendrogram level
		# this is necessary because the dendrogram object combines communities and the original node ids become lost
		partition_dendrogram = []
		# loop through dendrogram levels
		for level in range(len(dendrogram)):
			# initialize dictionary of communities for each node for this level
			level_partition = {}
			# loop through nodes
			for i in range(num_mol_1):
				# if level 0, make first dictionary
				if level == 0:
					# put parition value into level_partition dictionary
					level_partition[i] = dendrogram[level][i]
				else:
					# get the community from the previous level
					prev_comm = partition_dendrogram[level - 1][i]
					# use that comm and the previous level dictionary to get new comm
					level_partition[i] = dendrogram[level][prev_comm]
			# add dictionary for this level to the list of community dictionaries
			partition_dendrogram += [level_partition]
		# initialize community list, length of total mols in largest cluster with entries being their community
		# these are no longer using global mol index!!!
		comms = []
		level_zero_comms = []
		# record the partition each molecule in the largest cluster belongs to
		for i in range(num_mol_1):
			comms = comms + [partition_dendrogram[dend_level][i]]
			level_zero_comms = level_zero_comms + [partition_dendrogram[0][i]]
		# make dictionary of all communities which lists their molecules
		# dictionary for the specified dendrogram level partition
		community_dict = {}
		# and dictionary for level zero (first pass) partition
		community_dict_level_zero = {}
		# put empty list for every community into dictionary
		num_comms = max(comms)
		num_comms_array.append(num_comms)
		# and the same for level zero
		level_zero_num_comms = max(level_zero_comms)
		level_zero_num_comms_array.append(level_zero_num_comms)
		# don't remember right now why this is + 1
		for k in range(num_comms + 1):
			community_dict[k] = []
		for k in range(level_zero_num_comms + 1):
			community_dict_level_zero[k] = []
		# loop through mols and put into community in dictionary
		for j in range(num_mol_1):
			if community_dict[partition_dendrogram[dend_level][j]] == []:
				community_dict[partition_dendrogram[dend_level][j]] = [j]
			else:
				community_dict[partition_dendrogram[dend_level][j]] = community_dict[partition_dendrogram[dend_level][j]] + [j]
		# generate the community dictionary for the level 0 dendrogram partition
		for j in range(num_mol_1):
			if community_dict_level_zero[partition_dendrogram[0][j]] == []:
				community_dict_level_zero[partition_dendrogram[0][j]] = [j]
			else:
				community_dict_level_zero[partition_dendrogram[0][j]] = community_dict_level_zero[partition_dendrogram[0][j]] + [j]
		for comm in community_dict:
			# off by one because 1-indexed
			comm_size_dist[len(community_dict[comm]) - 1] += 1.0
			level_comm_size.append(len(community_dict[comm]))
		# write this community to file for use later
		with open('comm_list_t=%s.txt' % t,'w') as comm_list_file:
			for comm in community_dict:
				comm_list_line = ''
				for mol_id in community_dict[comm]:
					comm_list_line += '%s,' % str(mol_id)
				comm_list_file.write(comm_list_line[:-1] + '\n')
		with open('comm_list_level_0_t=%s.txt' % t,'w') as comm_list_level_0_file:
			for comm in community_dict_level_zero:
				comm_list_line = ''
				for mol_id in community_dict_level_zero[comm]:
					comm_list_line += '%s,' % str(mol_id)
				comm_list_level_0_file.write(comm_list_line[:-1] + '\n')
		for comm in community_dict_level_zero:
			# off by one because 1-indexed
			level_zero_comm_size_dist[len(community_dict_level_zero[comm]) - 1] += 1.0
			level_zero_comm_size.append(len(community_dict_level_zero[comm]))
		# obtain and store modularity values
		level_zero_modularity_list.append(community.modularity(dendrogram[0], NX_AM))
		level_modularity_list.append(community.modularity(level_partition, NX_AM))
		# run vmd to show communities
		# make representation file
		# time index matches gro files
		make_rep_file(community_dict,num_comms,t)
		#make_rep_file(community_dict_level_zero,level_zero_num_comms,(t - start_t)/sampling_dt)
		# run vmd
		make_vmd_plot(sys,mol_type_1,mol_type_2,t)
		make_vmd_plot(sys,mol_type_1,mol_type_2,t)
		# next time step
		t += sampling_dt
	### analyze community structures
	t = start_t
	ave_pddf = []
	comm_rg_values = []
	tot_num_comms = 0
	while t <= end_t:
		# get mol_1 coords for this time step
		mol_coords = {}
		with open('../coords/%s_%s.gro' % (mol_type_1,int((t - start_t)/sampling_dt))) as coord_file:
			for i,line in enumerate(coord_file):
				if i > 1 and i <= (num_mol_1 + 1):
					# mols are zero-indexed, so change line number accordingly because coords start at line number 2
					mol_coords[i - 2] = [float(line.split()[-3]),float(line.split()[-2]),float(line.split()[-1])]
				# get pbc size for analysis
				elif i == num_mol_1 + num_mol_2 + 2:
					pbc_size = float(line.split()[0])
		# collect community mol_ids and collect their coordinates
		community_lists = {}
		community_coords = {}
		with open('comm_list_t=%s.txt' % t) as comm_lists:
			# loop through lines in file (each line is a community)
			for i,line in enumerate(comm_lists):
				tot_num_comms += 1
				# save mol_ids for this community
				community_lists[i] = [int(mol) for mol in line.split(',')]
				# get coords for this community's mols
				community_coords[i] = [mol_coords[int(mol)] for mol in line.split(',')]
		# loop through communities for analysis
		for comm_num in range(len(community_lists)):
			# don't calculate pddf if only one molecule in COM... but add value of 0.0 to radius of gyration...
			# the zero pddf will be accounted for later when summed pddf is normalized by number of communitities
			if len(community_coords[comm_num]) < 2:
				comm_rg_values += [0.0]
			else:
				# calculate pddf and r_g for community
				comm_pddf, comm_pddf_bins, comm_rg = get_pddf_and_rg(community_coords[comm_num], pbc_size)
				comm_rg_values += [comm_rg]
				# add to average pddf
				if len(ave_pddf) == 0:
					ave_pddf = comm_pddf
					ave_pddf_bins = comm_pddf_bins
				else:
					for bin_val in range(len(ave_pddf_bins)):
						ave_pddf[bin_val] += comm_pddf[bin_val]
		# save data from this community
		t += sampling_dt
	# normalize ave pddf by number of comms
	for bin_val in range(len(ave_pddf)):
		ave_pddf[bin_val] = ave_pddf[bin_val]/float(tot_num_comms)
	plt.scatter(ave_pddf_bins, ave_pddf)
	plt.show()
	###
	# save statistics for cluster and community sizes
	if os.path.exists('comm_output') == False:
		os.mkdir('comm_output')
	os.chdir('comm_output')
	# average properties over number of communities or number of communities
	np.savetxt('comm_ave_pddf_bins.txt',ave_pddf_bins,delimiter=' ')
	np.savetxt('comm_ave_pddf.txt',ave_pddf,delimiter=' ')
	with open('comm_ave_rg.txt','w') as rg_out:
		rg_out.write('ave comm rg: %s +/- %s' % (np.average(comm_rg_values),np.std(comm_rg_values)))
	for mol in range(num_mol_1 - 1):
		cluster_size_dist[mol] = cluster_size_dist[mol]/(float(end_t - start_t)/sampling_dt + 1)
		comm_size_dist[mol] = comm_size_dist[mol]/(float(end_t - start_t)/sampling_dt + 1)
		level_zero_comm_size_dist[mol] = level_zero_comm_size_dist[mol]/(float(end_t - start_t)/sampling_dt + 1)
	np.savetxt('cluster_dist.txt',cluster_size_dist,delimiter=' ')
	np.savetxt('comm_size_dist.txt',comm_size_dist,delimiter=' ')
	np.savetxt('level_zero_comm_size_dist.txt',level_zero_comm_size_dist,delimiter=' ')
	print('level 0 mod:', np.average(level_zero_modularity_list))
	print('max level mod:', np.average(level_modularity_list))
	print('ave level zero comm size:', np.average(level_zero_comm_size))
	print('ave max level comm size:', np.average(level_comm_size))
	print('ave comm rg: %s +/- %s' %  (np.average(comm_rg_values),np.std(comm_rg_values)))
	
if __name__ == '__main__':
  main()

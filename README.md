# Community-analyses

This suite of python scripts enables a wide variety of analyses based upon the identification of communities within networks of intermolecular interactions, specifically employing modularity optimization. 

author of these codes is Dr. Michael Servis, former post-doc in the Aurora Clark group and now a staff member at Argonne National Laboratory. Email is: mservis@anl.gov

